---
layout: markdown_page
title: "Product Direction - Fulfillment: Provision"
description: "The Provision team at GitLab focuses on providing a seamless activation experience for customers."
canonical_path: "/direction/fulfillment/provision/"
---
 
## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />
 
## Mission
Our mission is to provide a seamless customer experience in accessing GitLab subscriptions, add-ons and trials, while providing key license delivery and usage data to internal teams for data-driven insights.

## Overview
The Provision group owns the process of providing customers with access to the features of GitLab's subscriptions and add-ons. This includes paid subscriptions, add-ons and trials.

For GitLab customers, we provide purchase confirmation mailers, paid and trial feature activation/de-activation, and the ability to report subscription seat usage. For Self Managed customers, this includes license management, Cloud License sync and offline usage data reporting. For GitLab.com customers, this includes namespace association and usage data syncs. 
 
For GitLab team members working in support and operational roles, our group strives to reduce the need for their involvement in provisioning and subscription usage management. This is achieved through automating processes, clearer documentation, and customer empowerment.

## Categories
The Provision Group owns three Categories of work. Each category represents a separate focus area and direction to achieve the broader Provision mission.

| Category | Definition | Maturity | Direction Page | Roadmap |
| ---------|------------|----------|----------------|-------|
| `SaaS Provisioning` | Related to the provisioning of SaaS products, including GitLab.com paid and trial subscriptions, CI Minutes and Storage. | `Viable` | [Direction](/direction/fulfillment/saas-provisioning/) | [Epic Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=end_date_asc&layout=QUARTERS&timeframe_range_type=THREE_YEARS&label_name[]=Fulfillment+Roadmap&label_name[]=group::provision&label_name[]=Category:SaaS+Provisioning&progress=COUNT&show_progress=true&show_milestones=false&milestones_type=GROUP&show_labels=false) |
| `SM Provisioning` | Related to the provisioning of Self Managed subscriptions and the management of `seat link` data collected from these subscriptions. | `Viable` | [Direction](/direction/fulfillment/sm-provisioning/) | [Epic Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=end_date_asc&layout=QUARTERS&timeframe_range_type=THREE_YEARS&label_name[]=Fulfillment+Roadmap&label_name[]=group::provision&label_name[]=Category:SM+Provisioning&progress=COUNT&show_progress=true&show_milestones=false&milestones_type=GROUP&show_labels=false) |
| `Fulfillment Integrations & Visibility` | Related to the integrations between quote-to-cash systems internally, and external marketplaces for allowing customer purchases of GitLab products through resellers. | `Minimal` | [Direction](/direction/fulfillment/fulfillment-integrations-and-visibility/) | Epic Roadmap |


## Team Focus Areas
### Top Priorities FY25
Over the next 12 months, the Provision team has three primary objectives:

1. Support the release of new product offerings
1. Expand trial availability and experience
1. Improve the user seat assignment experience
1. Streamline the license generation process

#### Support release of new product offerings
One of Provision's primary focus areas this year will be supporting the release of new products and features. We assist with ensuring features are accurately provisioned to customers on GitLab.com, GitLab Dedicated, and self managed instances. The details of specific product releases are not yet generally available public information. Products that we supported through release in FY24 include:
 - [Duo Pro](https://about.gitlab.com/gitlab-duo/) for [GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/10336), [GitLab Dedicated](https://gitlab.com/groups/gitlab-org/-/epics/11715), and [self managed GitLab instances](https://gitlab.com/groups/gitlab-org/-/epics/10613)
 - [Enterprise Agile Planning](https://about.gitlab.com/pricing/) [user seats](https://gitlab.com/groups/gitlab-org/-/epics/11648)
 - [US-based and US Government Support](https://gitlab.com/groups/gitlab-org/-/epics/10944)

#### Expand and improve trial availability
As GitLab continues to expand it's feature portfolio, we recognize the importance of allowing customers to trial these features ahead of committing to a purchase. In FY24, we launched the ability for [GitLab.com Premium customers to trial Ultimate functionality](https://gitlab.com/groups/gitlab-org/-/epics/9549) within their existing namespace and workflows. This year, we will continue to expand on this experience. , In addition, we plan to introduce a trial for [GitLab Duo Pro](https://gitlab.com/groups/gitlab-org/-/epics/10988) for all deployment type. This objective will also cover trials for any future products delivered in FY25.

#### Improve Addon User seat assignment experience
[GitLab Duo Pro](https://about.gitlab.com/gitlab-duo/) and [Enterprise Agile Planning](https://about.gitlab.com/pricing/) are the first user-based GitLab product offerings. To support MVC release of these products, we introduced new user management functionality for assigning these add-ons. We plan to continue to iterate on this experience by introducing [user sorting and filtering](https://gitlab.com/groups/gitlab-org/-/epics/11276) and [the ability to assign users in bulk](https://gitlab.com/groups/gitlab-org/-/epics/11462).

### Other Attention Areas
Besides the top initiatives outlined in our 1 year plan, we have some additional areas of attention outlined below. For a comprehensive list of our upcoming and ongoing projects, check out our [GitLab Epic Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=start_date_asc&layout=QUARTERS&timeframe_range_type=THREE_YEARS&label_name%5B%5D=Fulfillment+Roadmap&label_name%5B%5D=group%3A%3Aprovision&progress=COUNT&show_progress=true&show_milestones=false&milestones_type=GROUP).




## Key team metrics

### Provision & Integration failures monitoring

To support GitLab's SOX compliance requirements the Provision team is responsible for routinely monitoring, triaging, and resolving errors [during license provisioning](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/main/doc/provision_tracking_system/failure_monitoring.md) and [with key financial integrations](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/main/doc/process/salesforce_and_zuora_sentry_issue_monitor.md). Prior to every milestone, engineers are asked to volunteer to take responsibility for monitoring these systems on week-by-week format with the [planning issue](https://gitlab.com/gitlab-org/fulfillment-meta/-/blob/059d88f3183b8310a074cfffab71824c02760ab0/.gitlab/issue_templates/provision_planning.md#provision-tracking-system-triage). Any open weeks are randomly assigned in the week before the milestone starts. Issues that arise on Friends & Family Days and weekend days are covered on the next following workday
