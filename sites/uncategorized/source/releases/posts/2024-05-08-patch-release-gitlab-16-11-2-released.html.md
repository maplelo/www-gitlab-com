---
title: "GitLab Patch Release: 16.11.2, 16.10.5, 16.9.7"
categories: releases
author: Rohit Shambhuni
author_gitlab: rshambhuni
author_twitter: gitlab
description: "Learn more about GitLab Patch Release: 16.11.2, 16.10.5, 16.9.7 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
canonical_path: '/releases/2024/05/08/patch-release-gitlab-16-11-2-released.html.md'
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 16.11.2, 16.10.5, 16.9.7 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important bug and security fixes, and we strongly recommend that all GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version.

GitLab releases fixes for vulnerabilities in dedicated patch releases. There are two types of patch releases:
scheduled releases, and ad-hoc critical patches for high-severity vulnerabilities. Scheduled releases are released twice a month on the second and fourth Wednesdays.
For more information, you can visit our [releases handbook](https://handbook.gitlab.com/handbook/engineering/releases/) and [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of GitLab release blog posts [here](/releases/categories/releases/).

For security fixes, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are dedicated to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest patch release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Security fixes

### Table of security fixes

| Title | Severity |
| ----- | -------- |
| [ReDoS in branch search when using wildcards](#redos-in-branch-search-when-using-wildcards) | High |
| [ReDoS in markdown render pipeline](#redos-in-markdown-render-pipeline) | Medium |
| [Redos on Discord integrations](#redos-on-discord-integrations) | Medium |
| [Redos on Google Chat Integration](#redos-on-google-chat-integration) | Medium |
| [Denial of Service Attack via Pin Menu](#denial-of-service-attack--via-pin-menu) | Medium |
| [DoS by filtering tags and branches via the API](#dos-by-filtering-tags-and-branches-via-the-api) | Medium |
| [MR approval via CSRF in SAML SSO](#mr-approval-via-csrf-in-saml-sso) | Medium |
| [Banned user from groups can read issues updates via the api](#banned-user-from-groups-can-read-issues-updates-via-the-api) | Medium |
| [Require confirmation before linking JWT identity](#require-confirmation-before-linking-jwt-identity) | Medium |
| [View confidential issues title and description of any public project via export](#view-confidential-issues-title-and-description-of-any-public-project-via-export) | Medium |
| [SSRF via Github importer](#ssrf-via-github-importer) | Low |


### ReDoS in branch search when using wildcards

An issue has been discovered in GitLab CE/EE affecting all versions starting from 15.7 prior to 16.9.7, starting from 16.10 prior to 16.10.5, and starting from 16.11 prior to 16.11.2. It was possible for an attacker to cause a denial of service by crafting unusual search terms for branch names.
This is a high severity issue (`CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H`, 7.5).
It is now mitigated in the latest release and is assigned [CVE-2024-2878](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-2878).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### ReDoS in markdown render pipeline

An issue has been discovered in GitLab CE/EE affecting all versions before 16.9.7, all versions starting from 16.10 before 16.10.5, all versions starting from 16.11 before 16.11.2. It was possible for an attacker to cause a denial of service using maliciously crafted markdown content.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`, 6.5).
It is now mitigated in the latest release and is assigned [CVE-2024-2651](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-2651).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### Redos on Discord integrations

An issue has been discovered in GitLab CE/EE affecting all versions starting from 16.9 prior to 16.9.7, starting from 16.10 prior to 16.10.5, and starting from 16.11 prior to 16.11.2. A problem with the processing logic for Discord Integrations Chat Messages can lead to a regular expression DoS attack on the server.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`, 6.5).
It is now mitigated in the latest release and is assigned [CVE-2023-6682](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-6682).

Thanks to `Anonymizer` for reporting this vulnerability through our HackerOne bug bounty program.


### Redos on Google Chat Integration

An issue has been discovered in GitLab CE/EE affecting all versions starting from 16.11 prior to 16.11.2. A problem with the processing logic for Google Chat Messages integration may lead to a regular expression DoS attack on the server.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`, 6.5).
It is now mitigated in the latest release and is assigned [CVE-2023-6688](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-6688).

Thanks to `Anonymizer` for reporting this vulnerability through our HackerOne bug bounty program.


### Denial of Service Attack via Pin Menu

An issue has been discovered in GitLab CE/EE affecting all versions starting from 15.11 prior to 16.9.7, starting from 16.10 prior to 16.10.5, and starting from 16.11 prior to 16.11.2. The pins endpoint is susceptible to DoS through a crafted request.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`, 6.5).
It is now mitigated in the latest release and is assigned [CVE-2024-2454](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-2454).

Thanks [ac7n0w](https://hackerone.com/ac7n0w) for reporting this vulnerability through our HackerOne bug bounty program.


### DoS by filtering tags and branches via the API

An issue has been discovered in GitLab CE/EE affecting all versions starting from 15.4 prior to 16.9.7, starting from 16.10 prior to 16.10.5, and starting from 16.11 prior to 16.11.2 where abusing the API to filter branch and tags could lead to Denial of Service.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:L`, 4.3).
It is now mitigated in the latest release and is assigned [CVE-2024-4539](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-4539).

This vulnerability was reported internally by a GitLab team member [Vasilii Iakliushin](https://gitlab.com/vyaklushin).


### MR approval via CSRF in SAML SSO

An issue has been discovered in GitLab EE affecting all versions from 16.7 before 16.9.7, all versions starting from 16.10 before 16.10.5, all versions starting from 16.11 before 16.11.2. An attacker could force a user with an active SAML session to approve an MR via CSRF.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:U/C:N/I:H/A:N`, 5.7).
It is now mitigated in the latest release and is assigned [CVE-2024-4597](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-4597).

This vulnerability was reported internally by a GitLab team member [joernchen](https://gitlab.com/joernchen).


### Banned user from groups can read issues updates via the api

An issue has been discovered in GitLab EE affecting all versions starting from 15.2 prior to 16.9.7, starting from 16.10 prior to 16.10.5, and starting from 16.11 prior to 16.11.2. It was possible to disclose updates to issues to a banned group member using the API.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:N/A:N`, 4.3).
It is now mitigated in the latest release and is assigned [CVE-2024-1539](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-1539).

Thanks [ashish_r_padelkar](https://hackerone.com/ashish_r_padelkar) for reporting this vulnerability through our HackerOne bug bounty program.


### Require confirmation before linking JWT identity

An issue has been discovered in GitLab CE/EE affecting all versions starting from 10.6 prior to 16.9.7, starting from 16.10 prior to 16.10.5, and starting from 16.11 prior to 16.11.2 in which cross-site request forgery may have been possible on GitLab instances configured to use JWT as an OmniAuth provider.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:U/C:H/I:H/A:N`, 6.4).
It is now mitigated in the latest release and is assigned [CVE-2024-1211](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-1211).

Thanks [sim4n6](https://hackerone.com/sim4n6) for reporting this vulnerability through our HackerOne bug bounty program.


### View confidential issues title and description of any public project via export

An issue has been discovered in GitLab CE/EE affecting all versions starting from 14.0 prior to 16.9.7, starting from 16.10 prior to 16.10.5, and starting from 16.11 prior to 16.11.2. It was possible to disclose via the UI the confidential issues title and description from a public project to unauthorised instance users.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N`, 6.5).
It is now mitigated in the latest release and is assigned [CVE-2024-3976](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-3976).

Thanks [ahacker1](https://hackerone.com/ahacker1) for reporting this vulnerability through our HackerOne bug bounty program.


### SSRF via Github importer

An issue has been discovered in GitLab CE/EE affecting all versions starting from 15.5 prior to 16.9.7, starting from 16.10 prior to 16.10.5, and starting from 16.11 prior to 16.11.2. GitLab was vulnerable to Server Side Request Forgery when an attacker uses a malicious URL in the markdown image value when importing a GitHub repository.
This is a low severity issue (`CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:U/C:L/I:N/A:N`, 2.6).
It is now mitigated in the latest release and is assigned [CVE-2023-6195](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-6195).

Thanks [imrerad](https://hackerone.com/imrerad) for reporting this vulnerability through our HackerOne bug bounty program.


## Bug fixes


### 16.11.2

* [ci: Remove license scanning job (16.11)](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/6888)
* [Backport 'Zoekt: Fix exact search mode' into 16.11](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/150488)
* [Return or display Gitlab version if GITLAB_KAS_VERSION is a SHA](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/150600)
* [Allow self-managed instances to require licensed seats for Duo Chat](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/151040)
* [Merge branch 'release-environment-notification' into '16-11-stable-ee'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/151533)
* [Changed the email validation for only encoded chars](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/151526)
* [Backport 'hide archived filter in search when project selected' 16.11](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/151547)
* [Cherry-pick MR 151750 into '16-11-stable-ee'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/151884)
* [Fix reconfigure failure if Redis node has Rails Sentinel config](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7571)

### 16.10.5

* [ci: Remove license scanning job (16.10)](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/6887)
* [Upgrade gRPC to v1.62.1](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/6890)
* [Return or display Gitlab version if GITLAB_KAS_VERSION is a SHA](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/150602)
* [Merge branch 'release-environment-notification' into '16-10-stable-ee'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/151535)
* [Changed the email validation for only encoded chars](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/151529)
* [Cherry-pick MR 151750 into '16-10-stable-ee'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/151904)

### 16.9.7

* [ci: Remove license scanning job (16.9)](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/6886)
* [Return or display Gitlab version if GITLAB_KAS_VERSION is a SHA](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/150605)
* [Merge branch 'release-environment-notification' into 'master'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/151539)
* [Changed the email validation for only encoded chars](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/151530)
* [Cherry-pick MR 151750 into '16-9-stable-ee'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/151908)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Patch Notifications

To receive patch blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [patch release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).

## We’re combining patch and security releases

This improvement in our release process matches the industry standard and will help GitLab users get information about security and
bug fixes sooner, [read the blog post here](https://about.gitlab.com/blog/2024/03/26/were-combining-patch-and-security-releases/).
