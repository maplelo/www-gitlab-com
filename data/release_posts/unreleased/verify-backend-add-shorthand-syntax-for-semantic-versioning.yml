---
features:
  secondary:
  - name: "Semantic version ranges for published CI/CD components"
    available_in: [core, premium, ultimate]
    documentation_link: 'https://docs.gitlab.com/ee/ci/components/#semantic-versioning'
    reporter: dhershkovitch
    stage: verify
    categories:
    - Pipeline Composition
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/450835'
    description: |
      When using a CI/CD catalog component, you might want to have it automatically use the latest version. For example, you don't want to have to manually monitor all the components you use, and manually switch to the next version every time there is a minor update or security patch. But using `~latest` is also a bit risky, because minor version updates could have undesired behavior changes, and major version updates have a higher risk of breaking changes.

      With this release, you can opt into using the latest major or minor version of a CI/CD component. For example, specify `2` for the component version, and you'll get all updates for that major version, like `2.1.1`, `2.1.2`, `2.2.0`, but not `3.0.0`. Specify `2.1` and you'll only get patch updates for that minor version, like `2.1.1`, `2.1.2`, but not `2.2.0`.
