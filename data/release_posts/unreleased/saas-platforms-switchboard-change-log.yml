---
features:
  primary:
  - name: "Change log for configuration changes made using Switchboard"
    available_in: [ultimate]  # Include all supported tiers
    gitlab_com: false
    documentation_link: 'https://docs.gitlab.com/ee/administration/dedicated/configure_instance.html#view-the-configuration-change-log'
    image_url: '/images/unreleased/switchboard-change-log.png'
    reporter: lbortins
    stage: platforms # Prefix this file name with stage-informative-title.yml
    categories:
      - 'GitLab Dedicated'
      - 'Switchboard'
    issue_url: 'https://about.gitlab.com/direction/saas-platforms/switchboard/#fy25-q1'
    description: |
     You can now view the status of configuration changes made to your GitLab Dedicated instance infrastructure using the Switchboard [configuration page](https://docs.gitlab.com/ee/administration/dedicated/configure_instance.html#configuration-changes-in-switchboard).

     All users with access to view or edit your tenant in Switchboard will be able to view changes in the Configuration Change log and track their progress as they are applied to your instance.

     Currently, the Switchboard configuration page and change log are available for changes like managing access to your instance by adding an [IP to the allow list](https://docs.gitlab.com/ee/administration/dedicated/configure_instance.html#ip-allowlist) or configuring your instance's [SAML settings](https://docs.gitlab.com/ee/administration/dedicated/configure_instance.html#saml).
    
     We will be extending this functionality to enable self-serve updates for additional configurations in [coming quarters](https://about.gitlab.com/direction/saas-platforms/switchboard/#fy25-q2).
